---TEST---
---KNN_Polygon_Postgis.txt---
 
---Základní charakteristika---
 
--Výběrové charakteristiky polohy--
 
 počet = 1000 (řádků), 1 (sloupec)
 aritmetický průměr = 7404.031000 
 geometrický průměr = 795.067298 
 harmonický průměr = 173.278833 
 modus = 27.000000 
 medián = 812.000000 
 minimum = 7.000000 
 maximum = 1224148.000000 
 dolní kvartil = 318.500000 
 horní kvartil = 1989.000000 
 
--Výběrové charakteristiky variability--
 
 rozptyl = 3581912185.412043 
 výběrový rozptyl = 3585497683.095138 
 variační rozpětí= 1224141.000000 
 kvartilové rozpětí = 1670.500000 
 směrodatná odchylka = 59849.078401 
 výběrová směrodatná odchylka = 59879.025402 
 koeficient kvartilové variace = 0.723944 
 
--Další výběrové charakteristiky--
 
 obecný moment I. řádu = 7404.031000 
 obecný moment II. řádu = 3636731860.461000 
 obecný moment III. řádu = 3887749933827964.500000 
 obecný moment IV. řádu = 4566023091284873300000.000000 
 centrální moment I. řádu = 0.000000 
 centrální moment II. řádu = 3581912185.412043 
 centrální moment III. řádu = 3807782280674278.000000 
 centrální moment IV. řádu = 4452070178326297200000.000000 
 šikmost = 17.762321 
 špičatost = 347.002124 
>> 