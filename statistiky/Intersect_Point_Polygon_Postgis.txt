---TEST---
---Intersect_Point_Polygon_Postgis.txt---
 
---Základní charakteristika---
 
--Výběrové charakteristiky polohy--
 
 počet = 1000 (řádků), 1 (sloupec)
 aritmetický průměr = 169.953000 
 geometrický průměr = 74.342510 
 harmonický průměr = 24.121293 
 modus = 6.000000 
 medián = 97.500000 
 minimum = 2.000000 
 maximum = 1592.000000 
 dolní kvartil = 28.000000 
 horní kvartil = 225.000000 
 
--Výběrové charakteristiky variability--
 
 rozptyl = 44240.068791 
 výběrový rozptyl = 44284.353144 
 variační rozpětí= 1590.000000 
 kvartilové rozpětí = 197.000000 
 směrodatná odchylka = 210.333233 
 výběrová směrodatná odchylka = 210.438478 
 koeficient kvartilové variace = 0.778656 
 
--Další výběrové charakteristiky--
 
 obecný moment I. řádu = 169.953000 
 obecný moment II. řádu = 73124.091000 
 obecný moment III. řádu = 49634731.479000 
 obecný moment IV. řádu = 44368658541.111000 
 centrální moment I. řádu = 0.000000 
 centrální moment II. řádu = 44240.068791 
 centrální moment III. řádu = 22169608.018803 
 centrální moment IV. řádu = 20796219458.740654 
 šikmost = 2.382508 
 špičatost = 10.625584 
>> 