import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Spatial_join_Polygon_Polygon_Postgis {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Connection connection1 = null;

		try {

			connection1 = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection1 != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		Statement stmt1 = connection1.createStatement();
		String from="public.america_polygon_valid";
		
		
		
		int i=0;
		int i2=0;
		long cas = 0;
		
			String s= ("SELECT a.id as first, b.id as second "+
					 "FROM "+from+ " as a, "+from+" as b "+
					 "where ST_intersects(a.geometry, b.geometry)  and a.id<10000 and b.id<10000");
			
			long start1 = System.nanoTime();
			ResultSet rs = stmt1.executeQuery(s);
			long end1 = System.nanoTime();
			cas= end1-start1;
			System.out.println();
			System.out.println(cas/1000000+" ms");
			
			while(rs.next()){
				i= rs.getInt("first");
				i2=rs.getInt("second");
				
				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Spatial_join_Polygon_Polygon_Postgis.txt", true)))
				{

			     	bw.write(String.valueOf(i)+";"+String.valueOf(i2));
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}
				
			}
			System.out.println("Done it");
	}

}
