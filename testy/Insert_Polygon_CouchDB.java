import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Insert_Polygon_CouchDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File polygon = new File("polygon_nove.txt");
			reader = new BufferedReader(new FileReader(polygon));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(polygon));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  

		
		long celkovy= 0;
		long cas= 0;
		
     	for(int i=0;i<pole.length;i++){
     		String radek = pole[i];
     		cas=0;
			int pocet=0;
			char znak = ',';
			for (int n=0;n<radek.length();n++){
				if(radek.charAt(n)==znak)
					pocet++;
			}
			String []pole1 = new String[pocet+1];
			pole1=radek.split(",");

			List<Double>[] arr1 = new ArrayList[pocet+1];
			for (int p=0;p<pole1.length;p++){
				ArrayList<Double> polygon = new ArrayList<Double>();
				String[]poleParametru1= new String[2];
				poleParametru1=pole1[p].split(" ");
				if (p==0){
					poleParametru1[0] = poleParametru1[0].replaceAll("POLYGON"+"\\("+"\\(","");
				}
				poleParametru1[1] = poleParametru1[1].replaceAll("\\)"+"\\)" , "");
				//System.out.println("1. "+poleParametru1[0]);
				//System.out.println("2. "+poleParametru1[1]);
				polygon.add(Double.parseDouble(poleParametru1[0]));
				polygon.add(Double.parseDouble(poleParametru1[1]));
				arr1[p]=polygon;

				}	
	
			  
			  long start1 = System.nanoTime();
			  String doc2 = "{ "+"\"type\" "+":"+" \"Feature\" , "+"\"geometry\" : {"
						+"\"type\""+": \"Polygon\","+"\"coordinates\" : "+Arrays.asList(arr1)
						+"}, \"properties\" : { \"poradi\" : "+i+" } }";
						System.out.println(doc2+"\n");	
			 long end1 = System.nanoTime();
			 cas=end1-start1;
			 celkovy= celkovy+cas;
     	}   
     	
 	       
 	        

	}

}
