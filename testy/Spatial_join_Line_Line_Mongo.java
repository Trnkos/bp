import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;


import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Spatial_join_Line_Line_Mongo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub


		//connect POSTGRES

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}


		//connect MONGO

		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		DB db = mongoClient.getDB("mydb");
		System.out.println("Connect to database successfully");
		DBCollection col =  db.getCollection("join_line"); 
		BasicDBObject regexQuery=null;
		DBCursor cursor = null;
		cursor = col.find().limit(100);
		//System.out.println(cursor);
		String []string= new String[10000];
		List<DBObject> documentArray = cursor.toArray();
		int m=0;
		for(DBObject it : documentArray) {
			string[m]=it.toString().substring(0, 48);
			m++;
		}
		Statement stmt = connection.createStatement();
		String sql = " SELECT ST_AsTEXT(geometry) from public.america_line where id<10000";
		ResultSet rs = stmt.executeQuery(sql);
		int i1=0;
		long celkovy=0;
		while(rs.next()){
		  i1++;
		}
		//System.out.println(i1);
		 sql = "SELECT ST_AsText(ST_Transform(ST_GeomFromText(ST_AsText(geometry),3857),4326)) AS txt FROM public.america_line where id<10000";
		 rs = stmt.executeQuery(sql);
		int j1 = 0;
		while(rs.next()){ 
		String geomVal = rs.getString("txt"); 
		   j1++;
		
		String radek = geomVal;
			int pocet=0;
			char znak = ',';
			for (int n=0;n<radek.length();n++){
				if(radek.charAt(n)==znak)
					pocet++;
			}

			String []pole1 = new String[pocet+1];
			pole1=radek.split(",");

			List<Double>[] arr = new ArrayList[pocet+1];

			for (int p=0;p<pole1.length;p++){
				ArrayList<Double> line = new ArrayList<Double>();
				String[]poleParametru1= new String[2];
				poleParametru1=pole1[p].split(" ");
				if (p==0){
					poleParametru1[0] = poleParametru1[0].replaceAll("LINESTRING"+"\\(","");
				}
				poleParametru1[1] = poleParametru1[1].replaceAll("\\)" , "");
				line.add(Double.parseDouble(poleParametru1[0]));
				line.add(Double.parseDouble(poleParametru1[1]));
				arr[p]=line;


		 }
		
			regexQuery=null;
			long start=System.nanoTime();
			regexQuery = new BasicDBObject();
			regexQuery.put("geometry",
					new BasicDBObject("$geoIntersects",
							new BasicDBObject("$geometry",
									new BasicDBObject("type", "LineString")
									.append("coordinates", Arrays.asList(arr)  ))
							));
			
			cursor = col.find(regexQuery);
			long end=System.nanoTime();
			int i2=cursor.size();
	
			
			long cas=(end-start);
			
			List<DBObject> documentArray1 = cursor.toArray();
			int m1=0;
			try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Spatial_join_Line_Line_Mongo.txt", true)))
			{
				for(DBObject it1 : documentArray1) {
					bw.write(string[m1]+";"+it1.toString().substring(0, 48));
					bw.newLine();
					bw.flush();
					m1++;
				}
			}
			catch (Exception f)
			{
				System.err.println("Do souboru se nepovedlo zapsat.");
			}
			celkovy=celkovy+cas;
		}
		

		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");

		
		
	}

}
