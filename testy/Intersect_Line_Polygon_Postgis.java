import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Intersect_Line_Polygon_Postgis {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File file = new File("linie_nove.txt");
			reader = new BufferedReader(new FileReader(file));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  


		Connection connection1 = null;

		try {

			connection1 = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection1 != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

		
		Statement stmt1 = connection1.createStatement();
		String from="public.america_polygon_valid";
	
		long cas = 0;
		long celkovy =0;
     	for(int i=0;i<pole.length;i++){
     		//System.out.println("Linie: "+i+" ");
     		cas=0;
     		String radek = pole[i];
     		String[]poleParametru= new String[2];
			poleParametru=radek.split(",");
		    
     		String s=("SELECT id FROM "+from+" WHERE ST_Intersects(geometry,ST_Transform(ST_SetSRID(ST_GeomFromText('"+radek+"'),4326),3857));");
     		//System.out.println(s);
     		long start1 = System.nanoTime();
     		ResultSet rs2 = stmt1.executeQuery(s);
     		long end1 = System.nanoTime();
     		cas=end1-start1;
     		if (i<10){
				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis_pre.txt", true)))
				{

					bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000000));
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}

			}
			else{


				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis.txt", true)))
				{

					bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000000));  
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}
			}
     		
     		
     		
     		
     		
     		int i2=0;
     		while(rs2.next()){
     			String s1= rs2.getString("id");
    			i2++;
     			if(i<10){
     				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis_pre_id.txt", true)))
						{
     						bw.write(String.valueOf(i+1)+";"+String.valueOf(i2)+";"+String.valueOf(s1));
							bw.newLine();
							bw.flush();
							

						}
					
						catch (Exception f)
						{
						
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
                 	}
     			else{
     				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis_id.txt", true)))
					{



						bw.write(String.valueOf(i-9)+";"+String.valueOf(i2)+";"+String.valueOf(s1));
						bw.newLine();
						bw.flush();

					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}
     				
     				
     			}
				
     				
     				
     				
     			}
     		 if (i2==0){
     				if(i<10){
         				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis_pre_id.txt", true)))
    						{
         						bw.write(String.valueOf(i+1)+";"+"neni");
    							bw.newLine();
    							bw.flush();
    							

    						}
    					
    						catch (Exception f)
    						{
    						
    							System.err.println("Do souboru se nepovedlo zapsat.");
    						}
                     	}
         			else{
         				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Line_Polygon_Postgis_id.txt", true)))
    					{


    						bw.write(String.valueOf(i-9)+";"+"neni");
    						bw.newLine();
    						bw.flush();

    					}
    					catch (Exception f)
    					{
    						System.err.println("Do souboru se nepovedlo zapsat.");
    					}
         				
         				
         			}
     	
     		 }
   	      celkovy=celkovy+cas;
     	}   
     	System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");
		}

	}
