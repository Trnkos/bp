import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Insert_Point_Postgis {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File point = new File("body_nove.txt");
			reader = new BufferedReader(new FileReader(point));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(point));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  


		Connection connection1 = null;

		try {

			connection1 = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection1 != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		Statement stmt1 = connection1.createStatement();
		String from="public.america_point";

		String where="";
		String sql2 = "SELECT COUNT(*) as i2 from "+from+" "+where;
		ResultSet rs2 = stmt1.executeQuery(sql2);
		int i2=0;
		while(rs2.next()){
			i2= rs2.getInt("i2");
		}

		long cas = 0;
		long celkovy =0;
		for(int i=0;i<pole.length;i++){
			cas = 0;
			String radek = pole[i];
			String[]poleParametru= new String[2];
			poleParametru=radek.split(",");

			String s=("INSERT INTO "+from+" VALUES (default,ST_Transform(ST_SetSRID(ST_MakePoint("+poleParametru[0]+","+poleParametru[1]+"),4326),3857))");
			long start1 = System.nanoTime();
			stmt1.executeUpdate(s);
			long end1 = System.nanoTime();
			cas= end1-start1;
			//  System.out.println((i+1)+"   :   "+(cas)/1000000+"   ms");
			if (i<10){
				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Point_Postgis_pre.txt", true)))
				{

					//bw.write("'POINT("); 

					bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000));
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}

			}
			else{


				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Point_Postgis.txt", true)))
				{

					//bw.write("'POINT("); 

					bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000));  
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}
			}
			//System.out.println((cas)/1000000);
			celkovy = celkovy+cas; 
		}   
		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");

		sql2 = "SELECT COUNT(*) as i3 from "+from+" "+where;
		rs2 = stmt1.executeQuery(sql2);
		int i3=0;
		while(rs2.next()){
			i3= rs2.getInt("i3");
		}
		System.out.println(i2+" "+i3);

		String s="DELETE FROM "+from+" WHERE id>"+i2+";";
		stmt1.executeUpdate(s);
		System.out.println("hotovo");


		sql2 = "SELECT COUNT(*) as i2 from "+from+" "+where;
		rs2 = stmt1.executeQuery(sql2);
		i2=0;
		while(rs2.next()){
			i2= rs2.getInt("i2");
		}
		System.out.println(i2);

	}

}
