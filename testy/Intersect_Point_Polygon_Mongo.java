import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


public class Intersect_Point_Polygon_Mongo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File point = new File("body_nove.txt");
			reader = new BufferedReader(new FileReader(point));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(point));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  



		//connect MONGO
		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		DB db = mongoClient.getDB("mydb");
		System.out.println("Connect to database successfully");
		DBCollection col =  db.getCollection("america_polygon_valid"); 
		long cas = 0;
		long celkovy =0;
		for(int i=0;i<pole.length;i++){
			//System.out.println("Bod: "+i+" ");
			cas=0;
			int i2=0;
			String radek = pole[i];
			String[]poleParametru= new String[2];
			poleParametru=radek.split(",");
			ArrayList<Double> point = new ArrayList<Double>();

			point.add(Double.parseDouble(poleParametru[0]));
			point.add(Double.parseDouble(poleParametru[1]));

			BasicDBObject regexQuery=null;

			//System.out.println(regexQuery);
			long start=System.nanoTime();
			regexQuery = new BasicDBObject();
			regexQuery.put("geometry",
					new BasicDBObject("$geoIntersects",
							new BasicDBObject("$geometry",
									new BasicDBObject("type", "Point")
									.append("coordinates", point  ))
							));
			DBCursor cursor = col.find(regexQuery);
			i2=cursor.size();
			long end=System.nanoTime();
			cas=cas+(end-start);
			
			List<DBObject> documentArray = cursor.toArray();
			int m=0;
			if (i<10){
				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo_pre.txt", true)))
				{

					bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000000));
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}

			}
			else{


				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo.txt", true)))
				{

					bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000000));  
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}
			}

			if(i2>0){
				if (i<10){
					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo_pre_id.txt", true)))
					{
						for(DBObject it : documentArray) {
							bw.write(String.valueOf(i+1)+";"+String.valueOf(m+1)+";"+it.toString().substring(0, 48));
							bw.newLine();
							bw.flush();
							m++;
						}
					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}
				}
				else{
					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo_id.txt", true)))
					{
						for(DBObject it : documentArray) {
							bw.write(String.valueOf(i-9)+";"+String.valueOf(m+1)+";"+it.toString().substring(0, 48));
							bw.newLine();
							bw.flush();
							m++;
						}
					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}


				}
			}
			else{
				if (i<10){
					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo_pre_id.txt", true)))
					{
						bw.write(String.valueOf(i+1)+";"+"neni");
						bw.newLine();
						bw.flush();
					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}
				}
				else{
					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Intersect_Point_Polygon_Mongo_id.txt", true)))
					{
						bw.write(String.valueOf(i-9)+";"+"neni");
						bw.newLine();
						bw.flush();
					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}	

				}

			}


			celkovy=celkovy+cas;
		}   
		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");



	}
}
