import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;


import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Spatial_join_Polygon_Polygon_Mongo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub


		//connect POSTGRES

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}


		//connect MONGO

		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		DB db = mongoClient.getDB("mydb");
		System.out.println("Connect to database successfully");
		DBCollection col =  db.getCollection("join_polygon"); 
		BasicDBObject regexQuery=null;
		DBCursor cursor = null;
		cursor = col.find();
		//System.out.println(cursor);
		String []string= new String[10000];
		List<DBObject> documentArray = cursor.toArray();
		String from="public.america_polygon_valid";
		int m=0;
		for(DBObject it : documentArray) {
			string[m]=it.toString().substring(0, 48);
			System.out.println(string[m]);
			m++;
		}
		Statement stmt = connection.createStatement();
		String sql = null;
		ResultSet rs=null ;
		long celkovy=0;
		long cas= 0;
		sql = "SELECT ST_AsText(ST_Transform(ST_GeomFromText(ST_AsText(geometry),3857),4326)) AS txt from "+from+" where id<=10000";
		rs = stmt.executeQuery(sql);
		int j2 = 0;
		String []poles = new String[10000];
		while(rs.next()){ 
			String geomVal = rs.getString("txt"); 
			//pole_2[j2]=geomVal;
			poles[j2]=geomVal;

			j2++;
		}
      for (int i=0;i<poles.length;i++){
    	    cas=0;
			String radek = poles[i];
			int pocet=0;
			char znak = ',';
			for (int n=0;n<radek.length();n++){
				if(radek.charAt(n)==znak)
					pocet++;
			}
			String []pole1 = new String[pocet+1];
			pole1=radek.split(",");

			List<Double>[] arr1 = new ArrayList[pocet+1];
			for (int p=0;p<pole1.length;p++){
				ArrayList<Double> polygon = new ArrayList<Double>();
				String[]poleParametru1= new String[2];
				poleParametru1=pole1[p].split(" ");
				if (p==0){
					poleParametru1[0] = poleParametru1[0].replaceAll("POLYGON"+"\\("+"\\(","");
				}
				poleParametru1[1] = poleParametru1[1].replaceAll("\\)"+"\\)" , "");
				//System.out.println("1. "+poleParametru1[0]);
				//System.out.println("2. "+poleParametru1[1]);
				polygon.add(Double.parseDouble(poleParametru1[0]));
				polygon.add(Double.parseDouble(poleParametru1[1]));
				arr1[p]=polygon;

				}	
    	  
    	  
    	  
    	  
		regexQuery=null;
		long start=System.nanoTime();
		regexQuery = new BasicDBObject();
		regexQuery.put("geometry",
				new BasicDBObject("$geoIntersects",
						new BasicDBObject("$geometry",
								new BasicDBObject("type", "Polygon")
								.append("coordinates", Arrays.asList(Arrays.asList(arr1))  ))
						));

		cursor = col.find(regexQuery);
		long end=System.nanoTime();
		int i2=cursor.size();


		cas=(end-start);

		List<DBObject> documentArray1 = cursor.toArray();
		int m1=0;
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Spatial_join_Polygon_Polygon_Mongo.txt", true)))
		{
			for(DBObject it1 : documentArray1) {
				bw.write(m1+";"+string[m1]+";"+it1.toString().substring(0, 48));
				bw.newLine();
				bw.flush();
				m1++;
			}
		}
		catch (Exception f)
		{
			System.err.println("Do souboru se nepovedlo zapsat.");
		}
		celkovy=celkovy+cas;
      }


		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");



	}

}
