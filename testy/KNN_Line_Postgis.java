import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;
import java.io.FileWriter;
import java.io.BufferedWriter;

import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class KNN_Line_Postgis {

	public static void main(String[] args) throws SQLException, IOException {

		//connect POSTGRES

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}


		// BUFFER 

		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File file = new File("body_nove.txt");
			reader = new BufferedReader(new FileReader(file));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
			}
			for (int i2=0;i2<pole.length;i2++){
				//System.out.println(i2+" "+pole[i2]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		Statement stmt = connection.createStatement();

		int i2=0;
		int delka = 10;
		long cas=0;
		long celkovy=0;
		int []vysledek = new int [10];
		String from = "public.america_line";
		for (int j=0;j<pole.length;j++){
			System.out.println(j);
			i2=0;
			cas=0;

			delka=10;
			while (i2==0){
				//	System.out.println(i2+" "+delka);
				String sql="SELECT id,geometry, ST_Distance(ST_Transform(ST_SetSRID(ST_Point("+pole[j]+"),4326),3857),geometry) as dist from "+from +
						" WHERE ST_DWithin(geometry,ST_Transform(ST_SetSRID(ST_Point("+pole[j]+"),4326),3857),"+Integer.toString(delka)+") "+
						"ORDER by dist limit 10";
				long start=System.nanoTime();
				ResultSet rs = stmt.executeQuery(sql);
				long end=System.nanoTime();
				cas= cas+(end-start); 
				//System.out.println(j+" "+(end-start)/1000000+" ms");
				while(rs.next()){
					vysledek[i2]=rs.getInt("id");
					i2++;
				}
				//System.out.println(i2+" "+delka);
				if(i2>=10){
					if (j<10){	
                     	try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Postgis_pre.txt", true)))
						{



							bw.write(String.valueOf(j+1)+";"+String.valueOf((cas)/1000000));
							bw.newLine();
							bw.flush();

						}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
                     	for (int m=0;m<vysledek.length;m++){
							try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Postgis_pre_id.txt", true)))
							{


								
								bw.write(String.valueOf(j+1)+";"+String.valueOf(m+1)+";"+String.valueOf(vysledek[m]));
								bw.newLine();
								
								bw.flush();
								

							}
						
							catch (Exception f)
							{
							
								System.err.println("Do souboru se nepovedlo zapsat.");
							}
                     	}
					}
					else{


						try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Postgis.txt", true)))
						{



							bw.write(String.valueOf(j-9)+";"+String.valueOf((cas)/1000000));  
							bw.newLine();
							bw.flush();

						}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
						for (int m=0;m<vysledek.length;m++){
							try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Postgis_id.txt", true)))
							{



								bw.write(String.valueOf(j-9)+";"+String.valueOf(m+1)+";"+String.valueOf(vysledek[m]));
								bw.newLine();
								bw.flush();

							}
							catch (Exception f)
							{
								System.err.println("Do souboru se nepovedlo zapsat.");
							}
						}
					}
				}
				else{
					i2=0;
				}




				delka = delka*10;


			}
			celkovy=celkovy+cas;
			//System.out.println(cas/1000000+"ms");
		}	
		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");





	}
}