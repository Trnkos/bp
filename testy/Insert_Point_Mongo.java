import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Insert_Point_Mongo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File point = new File("body_nove.txt");
			reader = new BufferedReader(new FileReader(point));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(point));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  

		//connect MONGO

		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		MongoDatabase database = mongoClient.getDatabase("mydb");
		System.out.println("Connect to database successfully");


		MongoCollection col =  database.getCollection("america_point");		
		long celkovy= 0;
		long cas= 0;
		
     	for(int i=0;i<pole.length;i++){
     		cas=0;
     		String radek = pole[i];
     		String[]poleParametru= new String[2];
			poleParametru=radek.split(",");
			
			ArrayList<Double> point = new ArrayList<Double>();

			point.add(Double.parseDouble(poleParametru[0]));
			point.add(Double.parseDouble(poleParametru[1]));
			
			
			
			
			long start1 = System.nanoTime();
			Document doc = new Document("type", "Feature")
					.append("geometry",
							new Document("type", "Point")
							.append("coordinates", point
									))
					.append("properties",
							new Document("name","insert") );
			 col.insertOne(doc);
			long end1 = System.nanoTime();
			 cas=end1-start1;
			 
			 if (i<10){
					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Point_Mongo_pre.txt", true)))
					{

						

						bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000000));
						bw.newLine();
						bw.flush();

					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}

				}
				else{


					try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Point_Mongo.txt", true)))
					{

						

						bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000000));  
						bw.newLine();
						bw.flush();

					}
					catch (Exception f)
					{
						System.err.println("Do souboru se nepovedlo zapsat.");
					}
				}
			 
		
     	    celkovy = celkovy+cas;
     	}   
     	
     	System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");
		
          Document query = new Document("type", "Feature")
            				.append("properties",
							new Document("name","insert") );
 	       
 	    
 	        
 	        col.deleteMany(query);    
 	        
 	   System.out.println("Hotovo");     

	}

}
