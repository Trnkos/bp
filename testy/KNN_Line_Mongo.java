import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;

import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;



public class KNN_Line_Mongo {

	public static void main(String[] args) throws SQLException, IOException, MongoException {

		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File point = new File("body_nove.txt");
			reader = new BufferedReader(new FileReader(point));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(point));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  





		//connect MONGO
		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		DB db = mongoClient.getDB("mydb");
		System.out.println("Connect to database successfully");
		DBCollection col =  db.getCollection("america_line"); 
		int i2=0;
		int delka = 10;
		long cas=0;
		long celkovy=0;
		//double delkarad= delka/6378512.966*360;
		for(int i=0;i<pole.length;i++){
			cas=0;
			i2=0;
			delka=10;
			String radek = pole[i];
			String[]poleParametru= new String[2];
			poleParametru=radek.split(",");
			String[]vysledek=new String[10];
			ArrayList<Double> point = new ArrayList<Double>();

			point.add(Double.parseDouble(poleParametru[0]));
			point.add(Double.parseDouble(poleParametru[1]));

			//point.add(Double.parseDouble("33.00"));
			//point.add(Double.parseDouble("35.00"));
			while(i2==0){
				//System.out.println(i2+" "+delka);
				BasicDBObject regexQuery=null;
				
				//System.out.println(regexQuery);
				long start=System.nanoTime();
				regexQuery = new BasicDBObject();
				regexQuery.put("geometry",
						new BasicDBObject("$near",
								new BasicDBObject("$geometry",
										new BasicDBObject("type", "Point")
										.append("coordinates", point  ))
								.append("$maxDistance", delka)));
				DBCursor cursor = col.find(regexQuery);
				cursor.limit(10);
				i2=cursor.size();
				long end=System.nanoTime();
				cas=cas+(end-start);

		
			     int m=0;
			     BasicDBObject s=null;

				if(i2>=10){
					List<DBObject> documentArray = cursor.toArray();
					
					if (i<10){	
						try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Mongo_pre.txt", true)))
						{
							bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000000));
							bw.newLine();
							bw.flush();

						}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
						try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Mongo_pre_id.txt", true)))
						{
								for(DBObject it : documentArray) {
								bw.write(String.valueOf(i+1)+";"+String.valueOf(m+1)+";"+it.toString().substring(22, 46));
								bw.newLine();
								bw.flush();
								m++;
								}
	}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
					}
					else{


						try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Mongo.txt", true)))
						{
							bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000000));  
							bw.newLine();
							bw.flush();
	}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
						try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/KNN_Line_Mongo_id.txt", true)))
						{

							for(DBObject it : documentArray) {
								bw.write(String.valueOf(i-9)+";"+String.valueOf(m+1)+";"+it.toString().substring(22, 46));
								bw.newLine();
								bw.flush();
								m++;
								}
					}
						catch (Exception f)
						{
							System.err.println("Do souboru se nepovedlo zapsat.");
						}
					}
				}
				else{
					i2=0;
					
				}		
					delka=delka*10;
				//delkarad=delkarad/6378512.966*360;

			}
			celkovy=celkovy+cas;
		}	
		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");
	}
}
