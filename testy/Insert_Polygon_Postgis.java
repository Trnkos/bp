import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Insert_Polygon_Postgis {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		BufferedReader reader = null;
		String []pole = new String [1];
		try {
			File vstup = new File("polygon_nove.txt");
			reader = new BufferedReader(new FileReader(vstup));

			String line;
			int i=0;
			while ((line = reader.readLine()) != null) {
				i++;
			}
			pole = new String [i];
			int i1=0;
			reader = new BufferedReader(new FileReader(vstup));
			while ((line = reader.readLine()) != null) {
				pole[i1]=line;
				i1++;
				//System.out.println(i1);
			}
		}catch(Exception e){
			System.err.println("Soubor se nepovedl precist.");
		}  

		Connection connection1 = null;

		try {

			connection1 = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection1 != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		Statement stmt1 = connection1.createStatement();
		String from="public.america_polygon_valid";

		String where="";
		String sql2 = "SELECT COUNT(*) as i2 from "+from+" "+where;
		ResultSet rs2 = stmt1.executeQuery(sql2);
		int i2=0;
		while(rs2.next()){
			i2= rs2.getInt("i2");
		}
		long cas = 0;
		long celkovy =0;
		for(int i=0;i<pole.length;i++){
			cas=0;
			String radek = pole[i];
			//System.out.println(radek);

			String s=("INSERT INTO "+from+" VALUES ("+(i+41000000)+",ST_Transform(ST_SetSRID(ST_GeomFromText('"+radek+"'),4326),3857))");
			long start1 = System.nanoTime();
			stmt1.executeUpdate(s);
			long end1 = System.nanoTime();
			cas=end1-start1;

			if (i<10){
				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Polygon_Postgis_pre.txt", true)))
				{



					bw.write(String.valueOf(i+1)+";"+String.valueOf((cas)/1000));
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}

			}
			else{


				try (BufferedWriter bw = new BufferedWriter(new FileWriter("vysledky/Insert_Polygon_Postgis.txt", true)))
				{



					bw.write(String.valueOf(i-9)+";"+String.valueOf((cas)/1000));  
					bw.newLine();
					bw.flush();

				}
				catch (Exception f)
				{
					System.err.println("Do souboru se nepovedlo zapsat.");
				}
			}
		//	System.out.println(i);

			celkovy=celkovy+cas;
		}   

		System.out.println();
		System.out.println(celkovy/1000000+" ms");
		System.out.println("Done it");

		sql2 = "SELECT COUNT(*) as i3 from "+from+" "+where;
		rs2 = stmt1.executeQuery(sql2);
		int i3=0;
		while(rs2.next()){
			i3= rs2.getInt("i3");
		}
		System.out.println(i2+" "+i3);

		String s="DELETE FROM "+from+" WHERE id>"+40945671+";";//posledni z neupraveneho
		stmt1.executeUpdate(s);
		System.out.println("hotovo");


		sql2 = "SELECT COUNT(*) as i2 from "+from+" "+where;
		rs2 = stmt1.executeQuery(sql2);
		i2=0;
		while(rs2.next()){
			i2= rs2.getInt("i2");
		}
		System.out.println(i2);


	}

}
