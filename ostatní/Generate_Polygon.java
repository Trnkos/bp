

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Generate_Polygon {

	public static void main(String[] args) throws SQLException, FileNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		
		
		
		int n = 1000;  //pocet bodu 1 az 1000
		int celkem=0;
		Statement stmt1 = connection.createStatement();
		
		String query="select sum(polygon_pocet) as txt from public.oblast;";
		ResultSet rs1 = stmt1.executeQuery(query);
		String b ="";
		while(rs1.next()){
		    b = "";
			b=rs1.getString("txt");
			System.out.println(b);
		}
		int pocet=Integer.parseInt(b);
		
		for (int id=1;id<=33;id++){
			query="select polygon_pocet as txt from public.oblast where id = "+id+";";
			rs1 = stmt1.executeQuery(query);
			while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				//System.out.println(b);
			}	
		int hustota=Integer.parseInt(b);
		//System.out.println(id+" "+hustota);
		double p = (n*((double) hustota/pocet)); 
		int pocet_gen = (int) Math.round(n*((double) hustota/pocet)); 
		//System.out.println("     "+pocet_gen);
	    celkem=celkem+pocet_gen;
		System.out.println("id: "+id+", celkem "+pocet_gen+" "+p);
		query="select st_xmax(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
		rs1 = stmt1.executeQuery(query);
		
		while(rs1.next()){
		    b = "";
			b=rs1.getString("txt");
			//System.out.println(b);
		}
	     double x_max=Double.parseDouble(b);
	   
	     query="select st_xmin(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				//System.out.println(b);
			}
		 double x_min=Double.parseDouble(b);
		
		 query="select st_ymax(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				//System.out.println(b);
			}
		 double y_max=Double.parseDouble(b);
		
		 query="select st_ymin(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				//System.out.println(b);
			}
		 double y_min=Double.parseDouble(b);
		 
		    double x;
			double y;
			//System.out.println("INSERT INTO public.point(id, geometry)");
			//System.out.print("VALUES");

			String polygon = "";
		   for (int i=0;i<pocet_gen;i++){
			    x = 0;
		        y = 0;
		        double xnew=0; double ynew=0;double x_start=0;double y_start=0;
		        double c_uhelrad = 0;
				int i2=1;
				double c_uhel = 0;
			boolean t = false;  
			
			do{

				if (i2==1){
					
					while (t==false){
						x = 0;
						y = 0;
					    x =x+Math.random()*(x_min-x_max)+x_max;
						y= y+Math.random()*(y_max-y_min)+y_min;
						// System.out.println(i2+" "+y+" r "+y1 );
						
					    query="SELECT ST_Within(ST_Transform(ST_SetSRID(ST_MakePoint("+x+","+y+"),4326),3857),b.geom) as t from public.oblast as b where id = "+id+";";
					     rs1 = stmt1.executeQuery(query);
					     while(rs1.next()){
								t=rs1.getBoolean("t");
							//	System.out.println(t);
								}
						 } 
					
				}
				else{
					double delka_useku= (Math.random()*1000+100);
					//System.out.println(delka+" m");
					double uhel = (Math.random()*0.25*Math.PI);
					c_uhelrad= c_uhelrad+uhel;
					c_uhel = c_uhelrad/(2*Math.PI)*360;
					
					
					//System.out.println(c_uhel);
					if(c_uhel<360){
					double deltax=(delka_useku*Math.cos(c_uhelrad)/6378512.966)*360;
					double deltay= (delka_useku*Math.sin(c_uhelrad)/6378512.966)*360;
					//System.out.println("i2: "+i2);
					//System.out.println(deltax+" "+deltay);
	                
					xnew=x+deltax;
					ynew=y+deltay;
					if (i2==2){
	                	x_start=xnew;
	                    y_start=ynew;
	                    polygon = "POLYGON((";
	                    }
					polygon = polygon+xnew+" "+ynew+",";
					}
				}
				i2++;
	        	
	        	
	        }while(c_uhel<360);
	        polygon=polygon+x_start+" "+y_start;
			try (BufferedWriter bw = new BufferedWriter(new FileWriter("polygon_nove1.txt", true)))
			{
			    bw.write(polygon+"))");
			    bw.newLine();
			    bw.flush();
			}
			catch (Exception f)
			{
			    System.err.println("Do souboru se nepovedlo zapsat.");
			}
			
		
		}
		
		
   
		
		
	}
	 
		System.out.println(celkem);	
	}

}
