% skript na vykresleni dat
close all; clc; clear all;


data = dlmread('vysledky/Intersect_Point_Polygon_Postgis.txt');
poradi = data(:,1);
point_polygon_postgis = data(:,2);
data = dlmread('vysledky/Intersect_Line_Polygon_Postgis.txt');
line_polygon_postgis = data(:,2);
data = dlmread('vysledky/Intersect_Line_Line_Postgis.txt');
line_line_postgis = data(:,2);
data = dlmread('vysledky/Intersect_Polygon_Polygon_Postgis.txt');
polygon_polygon_postgis = data(:,2);
data = dlmread('vysledky/Intersect_Point_Polygon_Mongo.txt');
point_polygon_mongo = data(:,2);
data = dlmread('vysledky/Intersect_Line_Polygon_Mongo.txt');
line_polygon_mongo = data(:,2);
data = dlmread('vysledky/Intersect_Line_Line_Mongo.txt');
line_line_mongo = data(:,2);
data = dlmread('vysledky/Intersect_Polygon_Polygon_Mongo.txt');
polygon_polygon_mongo = data(:,2);

name = 'Intersect_Point_Polygon.png';

f = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,point_polygon_postgis','.b');
hold on;
semilogy(poradi,point_polygon_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost ur�en� pr�niku mezi nagenerovanou bodovou a polygonovou vrstvou');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� bodu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f,'-r600','-dpng',name)


name1 = 'Intersect_Line_Polygon.png';

f1 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,line_polygon_postgis','.b');
hold on;
semilogy(poradi,line_polygon_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost ur�en� pr�niku mezi nagenerovanou liniovou a polygonovou vrstvou');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� linie','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f1,'-r600','-dpng',name1)

name2 = 'Intersect_Line_Line.png';


f2 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,line_line_postgis','.b');
hold on;
semilogy(poradi,line_line_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost ur�en� pr�niku mezi nagenerovanou liniovou a liniovou vrstvou');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� linie','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14')
box off
print(f2,'-r600','-dpng',name2)

name3 = 'Intersect_Polygon_Polygon.png';


f3 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,polygon_polygon_postgis','.b');
hold on;
semilogy(poradi,polygon_polygon_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost ur�en� pr�niku mezi nagenerovanou polygonovou a polygonovou vrstvou');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� polygonu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f3,'-r600','-dpng',name3)