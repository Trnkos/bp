import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;

import java.util.*;

import org.bson.Document;
import org.ektorp.CouchDbConnector;  
import org.ektorp.CouchDbInstance;  
import org.ektorp.http.HttpClient;  
import org.ektorp.http.StdHttpClient;  
import org.ektorp.impl.StdCouchDbConnector;  
import org.ektorp.impl.StdCouchDbInstance;  
import org.ektorp.support.DesignDocument;  


public class Postgis_CouchDB_polygon {

	public static void main(String[] args) throws MalformedURLException, SQLException {
		// TODO Auto-generated method stub
		
		

		
		//connect POSTGRES

				//System.out.println("-------- PostgreSQL "
				//		+ "JDBC Connection Testing ------------");

				try {

					Class.forName("org.postgresql.Driver");

				} catch (ClassNotFoundException e) {

					System.out.println("Where is your PostgreSQL JDBC Driver? "
							+ "Include in your library path!");
					e.printStackTrace();
					return;

				}

				//System.out.println("PostgreSQL JDBC Driver Registered!");

				Connection connection = null;

				try {

					connection = DriverManager.getConnection(
							"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
							"poilkjmnb");

				} catch (SQLException e) {

				//	System.out.println("Connection Failed! Check output console");
					e.printStackTrace();
					return;

				}

				if (connection != null) {
				//	System.out.println("You made it, take control your database now!");
				} else {
				//	System.out.println("Failed to make connection!");
				}


				Statement stmt2 = connection.createStatement();
				long start=System.nanoTime();
				String sql2 = " SELECT ST_AsTEXT(geometry) from public.america_polygon_valid where id<5000000";
				ResultSet rs2 = stmt2.executeQuery(sql2);
				int i2=0;
				while(rs2.next()){
					i2++;
				}
				//System.out.println(i2);
				
				
				
				
				
				sql2 = "SELECT ST_AsText(ST_Transform(ST_GeomFromText(ST_AsText(geometry),3857),4326)) AS txt from public.america_polygon_valid where id<5000000";
				rs2 = stmt2.executeQuery(sql2);
				
				int j2 = 0;

				while(rs2.next()){ 
					String geomVal = rs2.getString("txt"); 
					//pole_2[j2]=geomVal;
					String radek=geomVal;
					j2++;
					
					int chyba1=0;
					//System.out.println(j2);
					//String radek =pole_2[m]; 
					int pocet=0;
					int pocet_zav=0;
					for (int n=0;n<radek.length();n++){
						if(radek.charAt(n)==')')
							pocet_zav++;

					}

					if(pocet_zav==2){
						char znak = ',';
						for (int n=0;n<radek.length();n++){
							if(radek.charAt(n)==znak)
								pocet++;

						}


						List<Double>[] arr1 = new ArrayList[pocet+1];
						String []pole1 = new String[pocet+1];
						pole1=radek.split(",");

						for (int p=0;p<pole1.length;p++){
							ArrayList<Double> polygon = new ArrayList<Double>();
							//System.out.println("pole "+pole1[p]);
							String[]poleParametru1= new String[2];
							poleParametru1=pole1[p].split(" ");
							if (p==0){
								poleParametru1[0] = poleParametru1[0].replaceAll("POLYGON"+"\\("+"\\(","");
							}
							poleParametru1[1] = poleParametru1[1].replaceAll("\\)"+"\\)" , "");
							//System.out.println("1. "+poleParametru1[0]);
							//System.out.println("2. "+poleParametru1[1]);
							polygon.add(Double.parseDouble(poleParametru1[0]));
							polygon.add(Double.parseDouble(poleParametru1[1]));
							arr1[p]=polygon;


						}
						/*Document doc2 = new Document("type", "Feature")
								.append("geometry",
										new Document("type", "Polygon")
										.append("coordinates", Arrays.asList(Arrays.asList(arr1))))
								.append("properties",
										new Document("name","polygon")
						.append("poradi",j2));*/
						
			
						
						String doc2 = "{ "+"\"type\" "+":"+" \"Feature\" , "+"\"geometry\" : {"
								+"\"type\""+": \"Polygon\","+"\"coordinates\" : "+Arrays.asList(Arrays.asList(arr1))
								+"}, \"properties\" : { \"poradi\" : "+j2+" } }";
								System.out.println(doc2+"\n");	
						
						
						
						
					}else{
						List<List<List<Double>>> arr2 = new ArrayList<List<List<Double>>>();

						String []pole1 = new String[pocet_zav-1];
						radek=radek.substring(0,radek.length()-2);
						//System.out.println(radek);
						pole1=radek.split("\\)");
						//System.out.println();
						for (int p=0;p<pole1.length;p++){
							//System.out.println(pole1[p]);
							String radek1= pole1[p];
							if (p==0){
								radek1=radek1.replaceAll("POLYGON"+"\\("+"\\(","");
							}else{
								radek1=radek1.replaceAll(","+"\\(","");
							}
							//pole1[p]=radek1;
							//System.out.println(radek1);
							//System.out.println();
							int pocet1=0;
							char znak = ',';
							for (int n=0;n<radek1.length();n++){
								if(radek1.charAt(n)==znak)
									pocet1++;

							}
							//List<Double>[] arr1 = new ArrayList[pocet1+1];
							List<List<Double>> arr1 = new ArrayList<List<Double>>();
							String []pole2 = new String[pocet1+1];
							pole2=radek1.split(",");
							for (int s=0;s<pole2.length;s++){
								ArrayList<Double> polygon = new ArrayList<Double>();
								//System.out.println("pole "+pole2[s]);
								String[]poleParametru1= new String[2];
								poleParametru1=pole2[s].split(" ");
								polygon.add(Double.parseDouble(poleParametru1[0]));
								polygon.add(Double.parseDouble(poleParametru1[1]));
								//arr1[s]=polygon;
								arr1.add(polygon);
							}

							arr2.add(arr1);


						}

						/*Document doc2 = new Document("type", "Feature")
								.append("geometry",
										new Document("type", "Polygon")
										.append("coordinates", arr2))
								.append("properties",
										new Document("name","polygon")
										.append("poradi",j2));
						*/
						String doc2 = "{ "+"\"type\" "+":"+" \"Feature\" , "+"\"geometry\" : {"
								+"\"type\""+": \"Polygon\","+"\"coordinates\" : "+arr2
								+"}, \"properties\" : { \"poradi\" : "+j2+" } }";
								System.out.println(doc2+"\n");	
						
						

					}



				}
		
		
	}

}
