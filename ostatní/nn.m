% skript na vykresleni dat
close all; clc; clear all;


data = dlmread('vysledky/NN_Point_Postgis.txt');
poradi = data(:,1);
point_postgis = data(:,2);
data = dlmread('vysledky/NN_Line_Postgis.txt');
line_postgis = data(:,2);
data = dlmread('vysledky/NN_Polygon_Postgis.txt');
polygon_postgis = data(:,2);
data = dlmread('vysledky/NN_Point_Mongo.txt');
point_mongo = data(:,2);
data = dlmread('vysledky/NN_Line_Mongo.txt');
line_mongo = data(:,2);
data = dlmread('vysledky/NN_Polygon_Mongo.txt');
polygon_mongo = data(:,2);

name = 'NN_Point.png';

f = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,point_postgis','.b');
hold on;
semilogy(poradi,point_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vyhled�n� nejbli���ho souseda v bodov� vrstv�');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� bodu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f,'-r600','-dpng',name)


name1 = 'NN_Line.png';

f1 = figure('visible','on','position',[0, 0, 1500, 700]);
semilogy(poradi,line_postgis','.b');
hold on;
semilogy(poradi,line_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vyhled�n� nejbli���ho souseda v liniov� vrstv�');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� bodu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f1,'-r600','-dpng',name1)

name2 = 'NN_Polygon.png';


f2 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,polygon_postgis','.b');
hold on;
semilogy(poradi,polygon_mongo,'+g');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vyhled�n� nejbli���ho souseda v polygonov� vrstv�');
legend({'PostGIS','MongoDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� bodu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f2,'-r600','-dpng',name2)