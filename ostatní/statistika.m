% skript statistiku
close all; clc; clear all;

nazev = 'Intersect_Polygon_Polygon_Postgis.txt';
predpona = 'vysledky/';
string = [predpona nazev];
data = dlmread(string);
poradi = data(:,1);
cas = data(:,2);

%Z�kladn� charakteristika


i=2;
disp('---TEST---')
pomlcka = '---';
string1 = [pomlcka nazev pomlcka];
disp(string1)
disp(' ')
disp('---Z�kladn� charakteristika---')
disp(' ')
disp('--V�b�rov� charakteristiky polohy--')
disp(' ')
pocet = size(cas);
disp(sprintf(' po�et = %d (��dk�), %d (sloupec)', pocet));
aritm_prumer = mean (cas);
disp(sprintf(' aritmetick� pr�m�r = %f ', aritm_prumer));
geom_prumer = geomean (cas);
disp(sprintf(' geometrick� pr�m�r = %f ', geom_prumer));
harm_prumer = harmmean (cas);
disp(sprintf(' harmonick� pr�m�r = %f ', harm_prumer));
modus = mode (cas);
disp(sprintf(' modus = %f ', modus));
median = median (cas);
disp(sprintf(' medi�n = %f ', median));
minimum = min(cas);
disp(sprintf(' minimum = %f ', minimum));
maximum = max(cas);
disp(sprintf(' maximum = %f ', maximum));
dolni_kvartil = quantile(cas,.25);
disp(sprintf(' doln� kvartil = %f ', dolni_kvartil));
horni_kvartil = quantile(cas,.75);
disp(sprintf(' horn� kvartil = %f ', horni_kvartil));
disp(' ');
disp('--V�b�rov� charakteristiky variability--')
disp(' ');
rozptyl = var (cas,1);
disp(sprintf(' rozptyl = %f ', rozptyl));
vyb_rozptyl = var (cas);
disp(sprintf(' v�b�rov� rozptyl = %f ', vyb_rozptyl));
var_rozpeti = maximum - minimum;
disp(sprintf(' varia�n� rozp�t�= %f ', var_rozpeti));
kvart_rozpeti = horni_kvartil - dolni_kvartil;
disp(sprintf(' kvartilov� rozp�t� = %f ', kvart_rozpeti));
smer_odchylka = std(cas,1);
disp(sprintf(' sm�rodatn� odchylka = %f ', smer_odchylka));
vyb_smer_odchylka = std(cas);
disp(sprintf(' v�b�rov� sm�rodatn� odchylka = %f ', vyb_smer_odchylka));
koef_kvart_variace = (horni_kvartil - dolni_kvartil) / (horni_kvartil + dolni_kvartil);
disp(sprintf(' koeficient kvartilov� variace = %f ', koef_kvart_variace));
disp(' ');
disp('--Dal�� v�b�rov� charakteristiky--')
disp(' ');
obecny_moment_prvni = sum(cas)/ pocet(1);
disp(sprintf(' obecn� moment I. ��du = %f ', obecny_moment_prvni));
obecny_moment_druhy = sum ((cas).^2) / pocet(1);
disp(sprintf(' obecn� moment II. ��du = %f ', obecny_moment_druhy));
obecny_moment_treti = sum ((cas).^3) / pocet(1);
disp(sprintf(' obecn� moment III. ��du = %f ', obecny_moment_treti));
obecny_moment_ctvrty = sum ((cas).^4) / pocet(1);
disp(sprintf(' obecn� moment IV. ��du = %f ', obecny_moment_ctvrty));
centr_moment_prvni = moment(cas,1);
disp(sprintf(' centr�ln� moment I. ��du = %f ', centr_moment_prvni));
centr_moment_druhy = moment(cas,2);
disp(sprintf(' centr�ln� moment II. ��du = %f ', centr_moment_druhy));
centr_moment_treti = moment(cas,3);
disp(sprintf(' centr�ln� moment III. ��du = %f ', centr_moment_treti));
centr_moment_ctvrty = moment(cas,4);
disp(sprintf(' centr�ln� moment IV. ��du = %f ', centr_moment_ctvrty));
sikmost = skewness(cas);
disp(sprintf(' �ikmost = %f ', sikmost));
spicatost = kurtosis(cas);
disp(sprintf(' �pi�atost = %f ', spicatost));
