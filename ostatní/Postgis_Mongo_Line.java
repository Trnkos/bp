

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.lang.Double;


import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;



public class Postgis_Mongo_Line {

	public static void main(String[] args) throws SQLException, IOException {

		//connect POSTGRES

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}


		//connect MONGO

		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		MongoDatabase database = mongoClient.getDatabase("mydb");
		System.out.println("Connect to database successfully");


		
		MongoCollection col =  database.getCollection("join_line"); 
		long start = System.nanoTime(); 
		Statement stmt = connection.createStatement();
		String sql = " SELECT ST_AsTEXT(geometry) from public.america_line where id<=10000";
		ResultSet rs = stmt.executeQuery(sql);
		int i1=0;
		while(rs.next()){
		  i1++;
		}
		System.out.println(i1);
		//String []pole_1 = new String [i1];
		 sql = "SELECT ST_AsText(ST_Transform(ST_GeomFromText(ST_AsText(geometry),3857),4326)) AS txt FROM public.america_line where id<=10000";
		 rs = stmt.executeQuery(sql);
		int j1 = 0;
		while(rs.next()){ 
		String geomVal = rs.getString("txt"); 
		   //pole_1[j1]=geomVal;
		   j1++;
		if (j1==8208735)
			System.out.println(geomVal);
		System.out.println(j1);
		//for (int m=0;m<pole_1.length;m++){
		//	System.out.println(m+1);
		//	String radek = pole_1[m];
		String radek = geomVal;
			int pocet=0;
			char znak = ',';
			for (int n=0;n<radek.length();n++){
				if(radek.charAt(n)==znak)
					pocet++;
			}
		//	System.out.println("pocet: "+pocet);

			String []pole1 = new String[pocet+1];
			pole1=radek.split(",");

			List<Double>[] arr = new ArrayList[pocet+1];

			for (int p=0;p<pole1.length;p++){
				ArrayList<Double> line = new ArrayList<Double>();
				String[]poleParametru1= new String[2];
				poleParametru1=pole1[p].split(" ");
				if (p==0){
					poleParametru1[0] = poleParametru1[0].replaceAll("LINESTRING"+"\\(","");
				}
				poleParametru1[1] = poleParametru1[1].replaceAll("\\)" , "");
				//System.out.println("1. "+poleParametru1[0]);
				//System.out.println("2. "+poleParametru1[1]);
				line.add(Double.parseDouble(poleParametru1[0]));
				line.add(Double.parseDouble(poleParametru1[1]));
				arr[p]=line;


				}
		
			Document doc1 = new Document("type", "Feature")
					.append("geometry",
							new Document("type", "LineString")
							.append("coordinates", Arrays.asList(arr)))
					.append("properties",
							new Document("name","linestring") );
			/*
			Document doc1 = new Document("type", "Feature")
					   .append("type", "LineString")
					   .append("geometry", Arrays.asList(arr));
			
			*/
			col.insertOne(doc1);
			
		}
		

			long end = System.nanoTime(); 
			System.out.println((end-start)/1000000+" ms");
			
		System.out.println("Done it!");
}
}

