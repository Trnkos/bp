


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.lang.Double;


import org.bson.Document;
import java.util.*;
//import java.util.logging;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.diagnostics.logging.Logger;



public class Postgis_Mongo_Point {

	public static void main(String[] args) throws SQLException, IOException {

		//connect POSTGRES

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://aether32.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}


		//connect MONGO

		MongoClient mongoClient = new MongoClient ("trnkabp.zcu.cz",27017);
		MongoDatabase database = mongoClient.getDatabase("mydb");
		System.out.println("Connect to database successfully");
		


		//POINT
		
        MongoCollection col =  database.getCollection("ahoj");
		long start = System.nanoTime();    
        Statement stmt = connection.createStatement();
		String sql = " SELECT ST_AsTEXT(geometry) from public.point";
		ResultSet rs = stmt.executeQuery(sql);
		int i=0;
		while(rs.next()){
			i++;
		}
		System.out.println(i);
		String []pole = new String [i];
		sql = "SELECT ST_AsText(ST_Transform(ST_GeomFromText(ST_AsText(geometry),3857),4326)) AS txt FROM public.point";
		rs = stmt.executeQuery(sql);
		int j = 0;
		while(rs.next()){ 
		String geomVal = rs.getString("txt"); 
		pole[j]=geomVal;
		j++;
		}

		for (int m=0;m<pole.length;m++){
			//System.out.println(pole[m]);
			String radek = pole[m];
			String[]poleParametru= new String[2];
			poleParametru=radek.split(" ");
			//System.out.println(poleParametru[0]);
			//System.out.println(poleParametru[1]);
			poleParametru[0] = poleParametru[0].replaceAll("POINT"+"\\(","");
			poleParametru[1] = poleParametru[1].replaceAll("\\)" , "");
			//System.out.println(poleParametru[0]);
			//System.out.println(poleParametru[1]);
			System.out.println(m);


			ArrayList<Double> point = new ArrayList<Double>();

			point.add(Double.parseDouble(poleParametru[0]));
			point.add(Double.parseDouble(poleParametru[1]));
			Document doc = new Document("type", "Feature")
					.append("geometry",
							new Document("type", "Point")
							.append("coordinates", point
									))
					.append("properties",
							new Document("name","point") );
			
		/*	Document doc = new Document("type", "Feature")
							   .append("type", "Point")
							   .append("geometry", point);*/
			
			col.insertOne(doc);

		}

		long end = System.nanoTime(); 
		System.out.println((end-start)/1000000+" ms");
		
	}
	}
		 
