

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Generate_Line {

	public static void main(String[] args) throws SQLException, FileNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://trnkabp.zcu.cz:5432/postgis", "trnka",
					"poilkjmnb");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		
		
		
		int n = 1000;  //pocet bodu 1 az 1000
		int celkem=0;
		Statement stmt1 = connection.createStatement();
		
		String query="select sum(line_pocet) as txt from public.oblast;";
		ResultSet rs1 = stmt1.executeQuery(query);
		String b ="";
		while(rs1.next()){
		    b = "";
			b=rs1.getString("txt");
			System.out.println(b);
		}
		int pocet=Integer.parseInt(b);
		
		for (int id=1;id<=33;id++){
			query="select line_pocet as txt from public.oblast where id = "+id+";";
			rs1 = stmt1.executeQuery(query);
			while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				
			}	
		int hustota=Integer.parseInt(b);
		
		double p = (n*((double) hustota/pocet)); 
		int pocet_gen = (int) Math.round(n*((double) hustota/pocet)); 
		
	    celkem=celkem+pocet_gen;
		System.out.println("id: "+id+", celkem "+pocet_gen+" "+p);
		query="select st_xmax(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
		rs1 = stmt1.executeQuery(query);
		
		while(rs1.next()){
		    b = "";
			b=rs1.getString("txt");
			
		}
	     double x_max=Double.parseDouble(b);
	   
	     query="select st_xmin(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				
			}
		 double x_min=Double.parseDouble(b);
		
		 query="select st_ymax(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
				//System.out.println(b);
			}
		 double y_max=Double.parseDouble(b);
		
		 query="select st_ymin(ST_Transform(geom,4326)) as txt from public.oblast where id = "+id+";";
	     rs1 = stmt1.executeQuery(query);
	     while(rs1.next()){
			    b = "";
				b=rs1.getString("txt");
			}
		 double y_min=Double.parseDouble(b);
		 
		    double x;
			double y;
			String line = "";
		   for (int i=0;i<pocet_gen;i++){
			   double delka = 0;
				int i2=1;
				x = 0;
				y = 0;
				double xnew=0; double ynew=0;double c_uhel = 0;
			boolean t = false;  
			
			do{

				if (i2==1){
					
					while (t==false){
						x = 0;
						y = 0;
					    x =x+Math.random()*(x_min-x_max)+x_max;
						y= y+Math.random()*(y_max-y_min)+y_min;
						xnew= x;
						ynew= y;
					
						line="LINESTRING(";
					    query="SELECT ST_Within(ST_Transform(ST_SetSRID(ST_MakePoint("+x+","+y+"),4326),3857),b.geom) as t from public.oblast as b where id = "+id+";";
					     rs1 = stmt1.executeQuery(query);
					     while(rs1.next()){
								t=rs1.getBoolean("t");
							
								}
						 } 
					if (t==true)
					     line = line+xnew+" "+ynew+",";
					
				}
				else{
					double delka_useku= (Math.random()*1000+100);
					delka = delka + delka_useku;
					
					double uhel = (Math.random()*0.25*Math.PI);
					if (Math.abs(c_uhel)<90)
						c_uhel = c_uhel+uhel/(2*Math.PI)*360;
					else
						c_uhel = c_uhel-uhel/(2*Math.PI)*360;	
					
					double deltax=(delka_useku*Math.cos(uhel)/6378512.966)*360;
					double deltay= (delka_useku*Math.sin(uhel)/6378512.966)*360;
					

					xnew=xnew+deltax;
					ynew=ynew+deltay;
                  if (delka<10000) 
					line = line+xnew+" "+ynew+",";
                  else
                	  line = line+xnew+" "+ynew;  
				}
				i2++;
				
			}while(delka <10000);
			
			try (BufferedWriter bw = new BufferedWriter(new FileWriter("linie_nove1.txt", true)))
			{
			
				
			    bw.write(line+")");
			    bw.newLine();
			    bw.flush();
			   
			}
			catch (Exception f)
			{
			    System.err.println("Do souboru se nepovedlo zapsat.");
			}
			
		
		}
		
		
   
		
		
	}
	 
		System.out.println(celkem);	
	}

}
