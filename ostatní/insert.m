% skript na vykresleni dat
close all; clc; clear all;

data = dlmread('vysledky/Insert_Point_Postgis.txt');
poradi = data(:,1);
point_postgis = data(:,2)/1000;
data = dlmread('vysledky/Insert_Line_Postgis.txt');
line_postgis = data(:,2)/1000;
data = dlmread('vysledky/Insert_Polygon_Postgis.txt');
polygon_postgis = data(:,2)/1000;
data = dlmread('vysledky/Insert_Point_Mongo.txt');
point_mongo = data(:,2)/1000;
data = dlmread('vysledky/Insert_Line_Mongo.txt');
line_mongo = data(:,2)/1000;
data = dlmread('vysledky/Insert_Polygon_Mongo.txt');
polygon_mongo = data(:,2)/1000;
data = dlmread('vysledky/Insert_Point_CouchDB.txt');
point_couch = data(:,2);
data = dlmread('vysledky/Insert_Line_CouchDB.txt');
line_couch = data(:,2);
data = dlmread('vysledky/Insert_Polygon_CouchDB.txt');
polygon_couch = data(:,2);


name = 'Insert_Point.png';

f = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,point_postgis','.b');
hold on;
semilogy(poradi,point_mongo,'+g');
hold on;
semilogy(poradi,point_couch,'*r');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vlo�en� bod� do vybran�ch S�BD');
legend({'PostGIS','MongoDB','CouchDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� bodu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f,'-r600','-dpng',name)


name1 = 'Insert_Line.png';

f1 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,line_postgis','.b');
hold on;
semilogy(poradi,line_mongo,'+g');
hold on;
semilogy(poradi,line_couch,'*r');

grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vlo�en� lini� do vybran�ch S�BD');
legend({'PostGIS','MongoDB','CouchDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� linie','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f1,'-r600','-dpng',name1)

name2 = 'Insert_Polygon.png';


f2 = figure('visible','on','position', [0, 0, 1500, 700]);
semilogy(poradi,polygon_postgis','.b');
hold on;
semilogy(poradi,polygon_mongo,'+g');
hold on;
semilogy(poradi,polygon_couch,'*r');
grid on;
%plot(semilogy(poradi,point_postgis','+b',poradi,point_mongo,'*g'))
%title('Rychlost vlo�en� polygon� do vybran�ch S�BD');
legend({'PostGIS','MongoDB','CouchDB'},...
    'Location','eastoutside','Orientation','vertical','Fontsize',13)
legend boxoff

xlabel('po�ad� polygonu','Fontsize',14)
ylabel('�as zpracov�n� [ms]','Fontsize',14)
box off
print(f2,'-r600','-dpng',name2)