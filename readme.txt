---Porovnání vybraných databázových systémů s podporou prostorových dat---

autor: Petr Trnka, 
       FAV ZČU, Plzeň 2020

---Obsah repozitáře---
 {Trnka_BP.pdf} - bakalářská práce
 {grafy}  	- složka s grafy
 {ostatní}      - složka s ostatními programy
 {readme.txt}   - popis a návod k použití
 {statistiky}   - složka se statistikami výsledků
 {testy}        - složka s implementovanými testy
	- Insert_Line_CouchDB.java
	- Insert_Line_Mongo.java
	- Insert_Line_Postgis.java
	- Insert_Point_CouchDB.java
	- Insert_Point_Mongo.java
	- Insert_Point_Postgis.java
	- Insert_Polygon_CouchDB.java
	- Insert_Polygon_Mongo.java
	- Insert_Polygon_Postgis.java
	- Intersect_Line_Line_Mongo.java
	- Intersect_Line_Line_Postgis.java
  	- Intersect_Line_Polygon_Mongo.java
	- Intersect_Line_Polygon_Postgis.java
	- Intersect_Point_Polygon_Mongo.java
     	- Intersect_Point_Polygon_Postgis.java
	- Intersect_Polygon_Polygon_Postgis.java
	- Intersect_Polygon_Polygon_Mongo.java
	- KNN_Line_Mongo.java
	- KNN_Line_Postgis.java
	- KNN_Point_Mongo.java
	– KNN_Point_Postgis.java
	– KNN_Polygon_Mongo.java
	– KNN_Polygon_Postgis.java
	– NN_Line_Mongo.java
	– NN_Line_Postgis.java
	– NN_Point_Mongo.java
	– NN_Point_Postgis.java
	– NN_Polygon_Mongo.java
	– NN_Polygon_Postgis.java
	– Spatial_join_Line_Line_Mongo.java
	– Spatial_join_Line_Line_Postgis.java
	– Spatial_join_Polygon_Polygon_Mongo.java
	– Spatial_join_Polygon_Polygon_Postgis.java
 {výsledky} - složka s výsledky testů

---Testy---

---Insert---

cíl: vložení záznamu

výsledek: Insert_Line_CouchDB.txt
	- Insert_Line_Mongo.txt
	- Insert_Line_Postgis.txt
	- Insert_Point_CouchDB.txt
	- Insert_Point_Mongo.txt
	- Insert_Point_Postgis.txt
	- Insert_Polygon_CouchDB.txt
	- Insert_Polygon_Mongo.txt
	- Insert_Polygon_Postgis.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku;čas vyhodnocení dotazu [ms])
Pozn. Pro testy Insert_xxx_Mongo.txt a Insert_xxx_Postgis.txt je čas vyhodnocení dotazu [s*10^(-6)].

---NN---

cíl: zjištění nejbližšího souseda

výsledek časový:– NN_Line_Mongo.txt
		– NN_Line_Postgis.txt
		– NN_Point_Mongo.txt
		– NN_Point_Postgis.txt
		– NN_Polygon_Mongo.txt
		– NN_Polygon_Postgis.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku;čas vyhodnocení dotazu [ms])

výpis nejbližšího souseda:– NN_Line_Mongo_id.txt
			  – NN_Line_Postgis_id.txt
			  – NN_Point_Mongo_id.txt
		  	  – NN_Point_Postgis_id.txt
			  – NN_Polygon_Mongo_id.txt
			  – NN_Polygon_Postgis_id.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku; id prvku)

---KNN---

cíl: zjištění k-nejbližšího souseda

výsledek časový:– KNN_Line_Mongo.txt
		– KNN_Line_Postgis.txt
		– KNN_Point_Mongo.txt
		– KNN_Point_Postgis.txt
		– KNN_Polygon_Mongo.txt
		– KNN_Polygon_Postgis.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku;čas vyhodnocení dotazu [ms])

výpis k-nejbližšího souseda:– KNN_Line_Mongo_id.txt
			    – KNN_Line_Postgis_id.txt
			    – KNN_Point_Mongo_id.txt
		  	    – KNN_Point_Postgis_id.txt
			    – KNN_Polygon_Mongo_id.txt
			    – KNN_Polygon_Postgis_id.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku; id prvku)

---Intersect---

cíl: zjištění průniku prvků

výsledek časový:- Intersect_Line_Line_Mongo.txt
		- Intersect_Line_Line_Postgis.txt
  		- Intersect_Line_Polygon_Mongo.txt
		- Intersect_Line_Polygon_Postgis.txt
		- Intersect_Point_Polygon_Mongo.txt
     		- Intersect_Point_Polygon_Postgis.txt
		- Intersect_Polygon_Polygon_Postgis.txt
		- Intersect_Polygon_Polygon_Mongo.txt

formát výsledku: uspořadaná dvojice: (pořadí prvku; čas vyhodnocení dotazu [ms])

výpis průniku:- Intersect_Line_Line_Mongo_id.txt
	      - Intersect_Line_Line_Postgis_id.txt
  	      - Intersect_Line_Polygon_Mongo_id.txt
	      - Intersect_Line_Polygon_Postgis_id.txt
	      - Intersect_Point_Polygon_Mongo_id.txt
     	      - Intersect_Point_Polygon_Postgis_id.txt
	      - Intersect_Polygon_Polygon_Postgis_id.txt
	      - Intersect_Polygon_Polygon_Mongo_id.txt

formát výsledku: uspořadaná trojice: (pořadí prvku, pořadí průniku, id prvku s průnikem)

---Spatial join---

cíl: zjištění protínajích se dvojic

výsledek průniku: – Spatial_join_Line_Line_Mongo.txt
		  – Spatial_join_Line_Line_Postgis.txt		
		  – Spatial_join_Polygon_Polygon_Mongo.txt
		  – Spatial_join_Polygon_Polygon_Postgis.txt

formát výsledku: uspořadaná dvojice: (id prvku, id prvku)

---Ostatní---

-obsahuje: -generátory prvků
	   -import dat do MongoDB a CouchDB
	   -.m soubory - tvorba grafů
	   -.m statistika



